<?php

use Illuminate\Database\Seeder;

class TypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('types')->delete();
        $types=array(
        	[
        		'type'=>'tech'

        	],
        	[
        		'type'=>'non-tech'
        	]


        	);

        DB::table('types')->insert($types);
    }
}
