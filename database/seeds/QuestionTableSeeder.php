<?php

use Illuminate\Database\Seeder;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('questions')->delete();

        $questions=array(
        	[
                'rank'=>'1',
        		'section_id'=>'1',
        		'ques_name'=>'Name',
                'input_type'=>'input',
        		'main'=>'Name: ',
        		'suggestion'=>''
        	],
            [
                'rank'=>'2',
                'section_id'=>'1',
                'ques_name'=>'Title',
                'input_type'=>'input',
                'main'=>'Title',
                'suggestion'=>''
            ],
            [
                'rank'=>'3',
                'section_id'=>'1',
                'ques_name'=>'Mobile no',
                'input_type'=>'input',
                'main'=>'Mobile no',
                'suggestion'=>''
            ],
            [
                'rank'=>'4',
                'section_id'=>'1',
                'ques_name'=>'Email',
                'input_type'=>'input',
                'main'=>'Email',
                'suggestion'=>''
            ],
            [
                'rank'=>'1',
                'section_id'=>'2',
                'ques_name'=>'Degree/Course',
                'input_type'=>'input',
                'main'=>'Degree/Course',
                'suggestion'=>''
            ],
            [
                'rank'=>'2',
                'section_id'=>'2',
                'ques_name'=>'Institute',
                'input_type'=>'input',
                'main'=>'Institute',
                'suggestion'=>''
            ],
             [
                'rank'=>'3',
                'section_id'=>'2',
                'ques_name'=>'Passing Year',
                'input_type'=>'input',
                'main'=>'Passing Year',
                'suggestion'=>''
            ],
             [
                'rank'=>'4',
                'section_id'=>'2',
                'ques_name'=>'Aggregate',
                'input_type'=>'input',
                'main'=>'Aggregate',
                'suggestion'=>''
            ],
            [
                'rank'=>'1',
                'section_id'=>'3',
                'ques_name'=>'Academic Achievements',
                'input_type'=>'textarea',
                'main'=>'Academic Achievements',
                'suggestion'=>''
            ],
            [
                'rank'=>'1',
                'section_id'=>'4',
                'ques_name'=>'',
                'input_type'=>'input',
                'main'=>'Company Name',
                'suggestion'=>''
            ],
             [
                'rank'=>'2',
                'section_id'=>'4',
                'ques_name'=>'',
                'input_type'=>'input',
                'main'=>'Role',
                'suggestion'=>''
            ],
             [
                'rank'=>'3',
                'section_id'=>'4',
                'ques_name'=>'',
                'input_type'=>'input',
                'main'=>'Start Date',
                'suggestion'=>''
            ],
             [
                'rank'=>'4',
                'section_id'=>'4',
                'ques_name'=>'',
                'input_type'=>'input',
                'main'=>'End Date',
                'suggestion'=>''
            ],
             [
                'rank'=>'5',
                'section_id'=>'4',
                'ques_name'=>'',
                'input_type'=>'textarea',
                'main'=>'Summary',
                'suggestion'=>''
            ],
             [
                'rank'=>'1',
                'section_id'=>'5',
                'ques_name'=>'',
                'input_type'=>'input',
                'main'=>'Title',
                'suggestion'=>''
            ],
            [
                'rank'=>'2',
                'section_id'=>'5',
                'ques_name'=>'',
                'input_type'=>'textarea',
                'main'=>'Summary',
                'suggestion'=>''
            ],
            [
                'rank'=>'3',
                'section_id'=>'5',
                'ques_name'=>'',
                'input_type'=>'input',
                'main'=>'Start Date',
                'suggestion'=>''
            ],
            [
                'rank'=>'4',
                'section_id'=>'5',
                'ques_name'=>'',
                'input_type'=>'input',
                'main'=>'End Date',
                'suggestion'=>''
            ],
            [
                'rank'=>'1',
                'section_id'=>'6',
                'ques_name'=>'',
                'input_type'=>'textarea',
                'main'=>'Skills',
                'suggestion'=>''
            ],
            [
                'rank'=>'1',
                'section_id'=>'7',
                'ques_name'=>'',
                'input_type'=>'textarea',
                'main'=>'Position Of Responsibility',
                'suggestion'=>''
            ],
            [
                'rank'=>'1',
                'section_id'=>'8',
                'ques_name'=>'',
                'input_type'=>'textarea',
                'main'=>'Extra Curricular',
                'suggestion'=>''
            ],
            [
                'rank'=>'1',
                'section_id'=>'9',
                'ques_name'=>'',
                'input_type'=>'textarea',
                'main'=>'Hobbies',
                'suggestion'=>''
            ]
            
        	);

        DB::table('questions')->insert($questions);
    }
}
