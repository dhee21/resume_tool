<?php

use Illuminate\Database\Seeder;

class SectionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('sections')->delete();

        $sections=array(
        	[
        		'section_name'=>'general',
                'type_id'=>'1',
                'section_rank'=>'1'
        	],
        	[
        	   'section_name'=>'education',
               'type_id'=>'1',
               'section_rank'=>'2'
        	],
            [
               'section_name'=>'Academic Achievements',
               'type_id'=>'1',
               'section_rank'=>'3'
            ],
            [
               'section_name'=>'Internships',
               'type_id'=>'1',
               'section_rank'=>'4'
            ],
        	[
        	  'section_name'=>'Projects',
              'type_id'=>'1',
              'section_rank'=>'5'
        	],
            [
               'section_name'=>'Skills and Tools',
               'type_id'=>'1',
               'section_rank'=>'6'
            ],
            [
               'section_name'=>'Position of Responsibility',
               'type_id'=>'1',
               'section_rank'=>'7'
            ],
            [
               'section_name'=>'Extra Curricular Activities',
               'type_id'=>'1',
               'section_rank'=>'8'
            ],
            [
               'section_name'=>'Interests',
               'type_id'=>'1',
               'section_rank'=>'9'
            ],

        	);

        DB::table('sections')->insert($sections);
    }
}
