<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateResumesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('resumes',function(Blueprint $table){
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('type_id');
            $table->integer('upvotes');
            $table->integer('views');
            $table->string('resume_name');
             $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('resumes');
    }
}
