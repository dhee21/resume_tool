<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuestionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('questions',function(Blueprint $table){
            $table->increments('id');
            $table->string('section_id');
            $table->string('ques_name');
            $table->string('rank');
            $table->string('main');
            $table->string('suggestion');
            $table->string('input_type');//input textarea etc
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('questions');
    }
}
