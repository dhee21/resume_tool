<!DOCTYPE html>
<html>
    <head>
        <title>Resume_tool</title>

       <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
                 <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}

     @section('stylesheets')
   
    <link rel='stylesheet' href="{{URL::asset('css/base.css')}}">
        @show
        
    </head>
    <body>

            @section('nav_bar')
          <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
            
                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Resume Builder
                </a>
                 <ul class="nav navbar-nav navbar-right">
                    <li><a href='{{URL("/your_resumes")}}'>Your Resumes</a></li>
                 </ul>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
               

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->

                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/new_logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
    @show
        
            @section('body')
            

            <a href="{{URL('/enter_name/1')}}"><div class='option-box btn middle'>TECH</div></a>
            <a href="{{URL('/enter_name/2')}}"><div class='option-box btn middle'>NON TECH</div></a>

            
                <div class="inner cover text-data">
                <h1 class="cover-heading"></t>CREATE PROFESSIONAL RESUMES</h1>
                <p class="lead">building good resumes really increases your chances for sitting in interviews<p>
                <p class="lead">
                  <a href="http://www.gingersoftware.com/content/writing-center/resume/" class="btn btn-lg btn-default link">Learn more</a>
                </p>
              </div>
           
            <!--<form action='/html' method='post' enctype="multipart/form-data">
            resume file: <input name='pdf_resume' type='file' ></input>
            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
            <button type='submit'>continue</button>
           </form>-->
            @show


<script>var Parameter={base_url:"{{URL('/')}}" };

</script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}


            @section('javascripts')
             
            @show
    </body>
</html>
