@extends('base')

@section('stylesheets')
<link rel='stylesheet' href='{{URL::asset("css/your_resumes.css")}}'>
@stop

@section('body')
@foreach($resume_arr as $resume)

		<div class='big_box col-md-3'>
			<div class='resume_info'>
				<p><h2>{{$resume->resume_name}}</h2></p>
			</div>
			<a href='show_preview/{{$resume->id}}/0'><button class='button btn btn-primary'>SEE</button></a>
			<a href='edit/{{$resume->id}}'><button class='button btn btn-warning'>EDIT</button></a>
			<a href='delete/{{$resume->id}}'><button class='button btn btn-danger'>DELETE</button></a>
		</div>
@endforeach
@stop