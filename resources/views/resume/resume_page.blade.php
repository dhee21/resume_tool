<!DOCTYPE html>
<html>
    <head>
        <title>Resume_tool</title>
	
 

@if(!$preview)
 <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
                 <!-- Fonts -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

    <!-- Styles -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">
    {{-- <link href="{{ elixir('css/app.css') }}" rel="stylesheet"> --}}


@endif
<!--<link rel='stylesheet' href="{{URL::asset('css/resume_page.css')}}">-->

</head>

	
<body>

@section('body')

@if(!$preview)
 <nav class="navbar navbar-default navbar-static-top">
        <div class="container">
            <div class="navbar-header">
            
                <!-- Branding Image -->
                <a class="navbar-brand" href="{{ url('/') }}">
                    Resume Builder
                </a>
                 <ul class="nav navbar-nav navbar-right">
                    <li><a href='{{URL("/your_resumes")}}'>Your Resumes</a></li>
                 </ul>
            </div>

            <div class="collapse navbar-collapse" id="app-navbar-collapse">
               

                <!-- Right Side Of Navbar -->
                <ul class="nav navbar-nav navbar-right">
                    <!-- Authentication Links -->
                    @if (Auth::guest())
                        <li><a href="{{ url('/login') }}">Login</a></li>
                        <li><a href="{{ url('/register') }}">Register</a></li>
                    @else
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                {{ Auth::user()->name }} <span class="caret"></span>
                            </a>

                            <ul class="dropdown-menu" role="menu">
                                <li><a href="{{ url('/new_logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
        </div>
    </nav>
@endif


<section class='resume_box' style='margin:10% 20% 10% 20%;border:solid black 2px'>


@foreach($resume_data as $section_id=>$section_data)

	<div class='section'>

		@foreach($section_data as $rank=>$rank_data)
				<div class='rank'>

			@foreach($rank_data as $ques_id=>$answer)
					<p class='answer' >{{$answer}}</p>
			@endforeach
				</div>
		@endforeach
	</div>	
@endforeach

</section>
	@if(!$preview)
	<button class='btn' style='width:10%;height:50px; margin-left:48%'><a href="{{$address}}">SEE PDF</a></button>
	@endif

@show



@if(!$preview)

<script>var Parameter={base_url:"{{URL('/')}}" };

</script>
<script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.3/jquery.min.js" integrity="sha384-I6F5OKECLVtK/BL+8iSLDEHowSAfUo76ZL9+kGAgTRdiByINKJaqTPH/QVNS1VDb" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
    {{-- <script src="{{ elixir('js/app.js') }}"></script> --}}
@endif


</html>