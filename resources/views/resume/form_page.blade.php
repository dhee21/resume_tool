@extends('base')

@section('stylesheets')
<link rel='stylesheet' href="{{URL::asset('css/form_page.css')}}">
@stop

@section('body')


<section ng-app="fillForm" id='page_box' ng-controller='fillForm' resumeId="{{$resume}}">


<section class='sections' >

@foreach($sections as $value)
<div class='btn sec_button' ng-click="showthis({{$value->id}})">{{$value->section_name}}</div>
@endforeach
	

</section>

<section class='form_area'>
	
@foreach($questions_array as $section=>$questions)

					<div class='{{$section}}' ng-show="section=={{$section}}" ><!--box enclosing whole section-->
							@if($section==4 || $section==5)

									
										<div class='question_box' ng-repeat='i in getTimes(no_{{$section}})'><button ng-click='subtract(i,section)'>-</button>
										 @foreach($questions as $value)<!--every question-->
										 
												 <div>{{$value->main}}</div>
												 <div>{{$value->suggestion}}</div>



												 				@if($value->input_type=='input')<!--{"project":{"1":{"q5":"imageprocessing","q6":"synop"},"2":{"q5":"vmock","q6":"synop"}}}-->
															 	<div><input type='text' ng-model='form_data[{{$section}}][i][{{$value->id}}]'/></div>


															 	@elseif($value->input_type=='textarea')
															 	<div><textarea type='text' ng-model='form_data[{{$section}}][i][{{$value->id}}]'></textarea></div>
									 							@endif
									 							<br><br>
												 	
										 
										 @endforeach
										 </div>
									

								
									<button ng-click='add({{$section}})'>+Add</button>

							@else

								 @foreach($questions as $value)<!--every question-->
										 <div class='question_box'>
												 <div>{{$value->main}}</div>
												 <div>{{$value->suggestion}}</div>

												 	@if($value->input_type=='input')
												 		<div><input type='text' ng-model='form_data[{{$section}}][1][{{$value->id}}]'/></div>
												 	@elseif($value->input_type=='textarea')
														<div><textarea type='text' ng-model='form_data[{{$section}}][1][{{$value->id}}]'></textarea></div>
												 	@endif
														
												 	<br><br>
										 </div>
								@endforeach

							@endif	

					</div>


					
@endforeach


</section>
<div>[[variable]]<div>
<!--<div>[[academic_data]]</div>
<div>[[form_data]]</div>
<div>[[variable]]<div>
<div>[[projects_data]]</div>-->




<button type='submit' ng-click='save_data()' class='btn left'>SAVE</button>

<button class='btn right' ><a href="{{URL('show_preview/[[resume_id]]/0')}}">PREVIEW</a></button>


</section>

@stop

@section('javascripts')


						<!--modules-->
			<script src="{{URL::asset('js/form_page.js')}}"></script>

						<!--controllers-->
			<script src="{{URL::asset('js/controllers/fillForm.js')}}"></script>

						<!--Directives-->
			<script src="{{URL::asset('js/directives/general.js')}}"></script>
			<script src="{{URL::asset('js/directives/acad.js')}}"></script>

@stop