<?php



/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::get('/', array('as'=>'dashboard','uses'=>'resumeController@dashboardAction'));

Route::get('/enter_name/{type}',array('as'=>'resume_name','uses'=>'resumeController@nameAction'));

Route::get('/your_resumes',array('as'=>'your_resumes','uses'=>'resumeController@yourResumesAction'));

Route::post('/form_page',array('as'=>'form_page','uses'=>'resumeController@formAction'));

Route::post('/save_data',array('as'=>'save','uses'=>'resumeController@saveAction'));

Route::post('/pdf_data',array('as'=>'pdf_data','uses'=>'resumeController@pdfAction'));

Route::post('/html',array('as'=>'html_data','uses'=>'resumeController@htmlAction'));

Route::get('show_preview/{resume_id}/{to_return}',array('as'=>'preview','uses'=>'resumeController@previewAction'));

Route::get('show_pdf/{resume_id}',array('as'=>'show_pdf','uses'=>'resumeController@showPdfAction'));

Route::get('new_logout',array('as'=>'new_logout','uses'=>'resumeController@newLogoutAction'));

Route::get('/session_data',array('as'=>'session_data','uses'=>'resumeController@sessionAction'));

Route::get('/delete/{resume_id}',array('as'=>'delete','uses'=>'resumeController@deleteAction'));

Route::get('/edit/{resume_id}',array('as'=>'edit','uses'=>'resumecontroller@editAction'));

Route::group(['middleware' => 'web'], function () {

Route::auth();

Route::get('/home', 'HomeController@index');


});