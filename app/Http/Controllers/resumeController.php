<?php

namespace App\Http\Controllers;
use Log;

use Illuminate\Routing\Controller;
use Illuminate\Http\Request;
use DB; 

use App\Http\Requests;
use Auth;
use Redirect;
use  Input;
use Validator;
use View;
use mPDF;

use App\models\Answer;
use App\models\Section;
use App\models\Question;
use App\models\Resume;
use App\models\History;
use App\models\User;

class resumeController extends Controller
{

  public function __construct()
  {
    $this->middleware('auth');
  }

public function dashboardAction(Request $request)
{
  
      $request->session()->pull('form_data');
      $request->session()->pull('resume_id');
      $request->session()->pull('type');
      $request->session()->pull('resume_name');
  
      
   return view('base');
}

public function nameAction(Request $request,$type)
{
  if($request->session()->has('type') && $type!=session('type'))
  {
      return redirect('/enter_name/'.session('type'));
  }
  $request->session()->put('type',$type);
if($request->session()->has('resume_name'))
{
    return view('resume.name_page',array('name'=>session('resume_name')));
}
  return view('resume.name_page',array('name'=>'No Name'));
}

public function yourResumesAction(Request $request)
{
  $request->session()->pull('form_data');
  $request->session()->pull('resume_id');
  $request->session()->pull('type');
  $request->session()->pull('resume_name');

  $user_id=Auth::user()->id;
  $user=new User;
  $user->id=$user_id;
  $resumes=$user->resumes;
  return view('resume.your_resumes',array('resume_arr'=>$resumes));

}

public function formAction(Request $request) 
{
    $resume_name=$request->request->get('resume_name');
    $request->session()->put('resume_name',$resume_name);
    $type_id=session('type');
    
    if($request->session()->has('resume_id'))
    {
      $resume_id=session('resume_id');
      $resume=Resume::find($resume_id);
      $resume->resume_name=$resume_name;
      $resume->save();
      
    }
    else
    {
      $resume=new Resume;
      $resume->type_id=$type_id;
      $resume->user_id=Auth::user()->id;
      $resume->resume_name=$resume_name;
      $resume->save();
      $resume_id=$resume->id;
      $request->session()->put('resume_id',$resume_id);
    }
    

      
      $sections=DB::table('sections')->where('type_id',$type_id)->orderBy('section_rank','asc')->get();
      $questions=array();
      foreach($sections as $section)
      {
        $instance=new Section;
        $instance->id=$section->id;
        $questions[$section->id]=$instance->questions;
      }

        return View::make('resume.form_page',array('sections'=>$sections,'questions_array'=>$questions,'resume'=>$resume_id));
}


public function saveAction(Request $request){
    $postdata = file_get_contents("php://input");
$data = json_decode($postdata);



$dynamic_data=$data->form_data;
$request->session()->put('form_data',$dynamic_data);

//return json_encode($dynamic_data);
$resume_id=session('resume_id');


           

            foreach($dynamic_data as $section_id=>$section_data)
            {
              $rank_largest=0;
              

                  foreach ($section_data as $rank => $questions) 
                  {
                        

                        $rank_largest=$rank;
                      foreach($questions as $q_id=> $value)
                      {

                        $present=DB::table('answers')->where('question_id',$q_id)
                                                         ->where('resume_id',$resume_id)
                                                         ->where('rank',$rank)
                                                         ->first();


                          if($present)
                          {

                              $content=Answer::find($present->id);
                              $content->content=$value;
                              $content->save();
                            
                              
                          }
                          else
                          {
                              $content=new Answer;
                              $content->question_id=$q_id;
                              $content->content=$value;
                              $content->resume_id=$resume_id;
                              $content->rank=$rank;
                              $content->save(); 
                          }

                      }
                      
                  }
                  
                     if($section_id>0)
                     {
                       $section_to_del=new Section;
                       $section_to_del->id=$section_id;
                       $arr_ques=$section_to_del->questions;//getting all question of this question's section
                       

                       foreach($arr_ques as $value)
                       {
                          $id_to_delete=$value->id;
                          $delete_rows=Answer::where('rank','>',$rank_largest)->where('question_id',$id_to_delete)->delete();
                       }
                     }

            }

            return json_encode('success');

}

public function pdfAction(Request $request){

            $parser = new \Smalot\PdfParser\Parser();
            $file=$request->files->get('pdf_resume');
            //return json_encode($file);

            $pdf    = $parser->parseFile($file);
            return nl2br($pdf->getText());
}


public function htmlAction(Request $request)
{
     
     $mpdf=new mPDF("en", "A4", "15");
     $file=$request->files->get('pdf_resume');
     $content=file_get_contents($file);
     $mpdf->WriteHTML($content);
  $mpdf->Output();

}



public function previewAction(Request $request,$resume_id,$to_return = 0)
{

        
        $filled=0;
        $resume=DB::table('resumes')->where('id',$resume_id)->first();
        if(Auth::user()->id != $resume->user_id)
        {
          return redirect()->route('dashboard');
        }

        $type=$resume->type_id;
        $sections=DB::table('sections')->where('type_id',$type)->orderBy('section_rank','asc')->get();

        $resume_data=[];
        foreach($sections as $value)
        {
          $section=new Section;
          $section->id=$value->id;
          $section->section_name=$value->section_name;
          $questions=$section->questions;

          foreach ($questions as $question) {
              $answers=$question->answers->where('resume_id',$resume_id);
              //return json_encode($answers);

              foreach ($answers as $answer) {
                      if($answer->content)
                      {
                        $filled=1;
                      }
                    $resume_data[$section->id][$answer->rank][$answer->question_id]=$answer->content;

                  }    
          }


        }

        if($to_return)
        {
          return $resume_data;
        }

        if(empty($resume_data) || !$filled)
        {
          $myResumeView='<h1>empty</h1>';
        }
        else
        {
          $myResumeView=view('resume.resume_page',array('resume_data'=>$resume_data,'preview'=>1))->render();
        }
        $history=new History;
        $history->resume_id=$resume_id;
        $history->data=$myResumeView;
        $history->save();

        $address='show_pdf/'.$resume_id;
        
        return view('resume.resume_page',array('resume_data'=>$resume_data,'preview'=>0,'address'=>URL($address)));
        
}

public function showPdfAction(Request $request,$resume_id)
{
      $latest_resume=DB::table('histories')->where('resume_id',$resume_id)->orderBy('id','desc')->first();
       $data=$latest_resume->data;
      $mpdf=new mPDF("en", "A4", "15");
      $mpdf->WriteHTML($data);
       return $mpdf->Output();
}

public function sessionAction(Request $request)
{
     if($request->session()->has('form_data'))
      {
        
        return json_encode(session('form_data'));
      }

    
}

public function deleteAction(Request $request,$resume_id)
{
      DB::table('resumes')->where('id',$resume_id)->delete();
      DB::table('answers')->where('resume_id',$resume_id)->delete();
      DB::table('histories')->where('resume_id',$resume_id)->delete();
      return redirect()->route('your_resumes');
}

public function editAction(Request $request,$resume_id)
{
 
  $resume=DB::table('resumes')->where('id',$resume_id)->first();
        if(Auth::user()->id != $resume->user_id)
        {
          return redirect()->route('dashboard');
        }
$resume_data=resumeController::previewAction($request,$resume_id,1);
$request->session()->put('form_data',$resume_data); 
$request->session()->put('resume_name',$resume->resume_name);
$request->session()->put('resume_id',$resume->id); 


return redirect('enter_name/'.$resume->type_id);

}

public function newLogoutAction(Request $request)
{
  $request->session()->pull('form_data');
  $request->session()->pull('resume_id');
  $request->session()->pull('type');
  $request->session()->pull('resume_name');
  return redirect('/logout');
}


}
