<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;


class Section extends Model
{
    
    public function questions()
    {
    	return $this->hasMany('App\models\Question');
    }
}
