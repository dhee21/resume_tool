<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    //
    public function resumes()
    {
    	return $this->hasMany('App\models\Resume');
    }
}
