<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    //
    public function section()
    {
    	return $this->belongsTo('App\models\Section');
    }

    public function answers()
    {
    	return $this->hasMany('App\models\Answer')->orderBy('rank');
    }
}
